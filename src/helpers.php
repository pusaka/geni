<?php /*** Bismillahirrahmanirrahim ***/

use Ramsey\Uuid\Uuid;

if (! function_exists('oid_uuid')) {
    function oid_uuid(...$tree) {
        $oid = implode('.', $tree);

        return $oid ?
            Uuid::uuid5(Uuid::NAMESPACE_OID, $oid)->toString() : Uuid::NIL;
    }
}