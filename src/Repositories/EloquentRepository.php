<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EloquentRepository
 * @package Pusaka\Geni
 */
class EloquentRepository implements ResourceEntityRepository
{
    use EloquentRelationshipRepository;

    /**
     * Eloquent model of repository
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Fill repository with a model
     *
     * @param Illuminate\Database\Eloquent\Model $model eloquent model
     */
    final public function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Take repository model
     *
     * @return Illuminate\Database\Eloquent\Model
     */

    public function getModel()
    {
        return $this->model;
    }

    public function makeModel()
    {
        return app()->make($this->getTypeKey());
    }

    /**
     * Get fresh model of repository
     *
     * @param  Illuminate\Database\Eloquent\Model|mixed|null $key
     * @return Illuminate\Database\Eloquent\Model
     */
    public function renewModel($key = null)
    {
        if ($key == null) {
            $model = $this->makeModel();
        }
        elseif ($key instanceof Model) {
            $model = $key;
        }
        else {
            $model = $this->find($key);
        }

        return $this->setModel($model);;
    }

    /**
     * Save a new entity in repository
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->renewModel();
        $attributes = $data['attributes'] ?? [];
        $this->model->fill($attributes);

        isset($data['relationships']) ?
            $this->relatingModelBelongsTo($data['relationships'])
            : null ;

        $this->model->save();

        isset($data['relationships']) ?
            $this->relatingModelHas($data['relationships'])
            : null ;

        return $this->model;
    }

    /**
     * Retrieve data of repository
     *
     * @param array $columns
     * @return mixed
     */
    public function retrieve($columns = ['*'], $pageSize = null)
    {
        $this->renewModel();
        $builder = $this->model->newQuery();
        $this->extendRetrieving($builder);

        if ($pageSize) {
            $collection = $builder->paginate($pageSize, $columns);
        }
        else {
            $collection = $builder->get($columns);
        }

        return $collection;
    }

    /**
     * Prepare query builder for retrieving
     * @return void
     */
    protected function extendRetrieving(Builder $query)
    {
        $this->extendRetrieval($query);
    }

    /**
     * Deprecated
     * @return void
     */
    public function extendRetrieval(Builder $query)
    {
        //
    }

    /**
     * Retrieve whole data
     * @param  array  $columns
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function retrieveAll($columns = ['*'])
    {
        return $this->retrieve($columns);
    }

    /**
     * Retrieve paginated data
     * @param  array  $columns
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function retrievePaginated($pageSize = 15, $columns = ['*'])
    {
        return $this->retrieve($columns, $pageSize);
    }

    /**
     * Find data by key
     *
     * @param       $key
     * @param array $columns
     *
     * @return mixed
     */
    public function find($key, $columns = ['*']) {
        $this->renewModel();
        $builder = $this->model->newQuery();
        $builder->where($this->getModelIdKey(), $key);
        $this->extendRetrieval($builder);

        return $builder->firstOrFail();
    }

    protected function getModelIdKey()
    {
        $idKey = ($this->model instanceof Model) ?
            $this->model->getRouteKeyName() : 'id';

        return $idKey;
    }

    public function extendCreating()
    {
        # code...
    }

    /**
     * Update a entity in repository by key
     *
     * @param array $data
     * @param       $key
     *
     * @return mixed
     */
    public function update(array $data, $key)
    {
        $this->renewModel($key);

        isset($data['attributes']) ?
            $this->model->fill($data['attributes'])
            : null ;

        isset($data['relationships']) ?
            $this->relatingModelBelongsTo($data['relationships'])
            : null ;

        $this->model->save();

        isset($data['relationships']) ?
            $this->relatingModelHas($data['relationships'])
            : null ;

        return $this->model;
    }

    /**
     * Delete a entity in repository by key
     *
     * @param $key
     *
     * @return int
     */
    public function delete($key)
    {
        $this->renewModel($key);
        $this->model->delete();

        return true;
    }
}
