<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Exceptions;

use Illuminate\Validation\ValidationException as BaseException;

/**
 * ValidationException
 */
class ValidationException extends BaseException
{
    public function render()
    {
        $e = [];

        foreach ($this->errors() as $field => $error) {
            array_push($e, $this->formatValidationError('Invalid Attribute', $error, $field));
        }

        return response()->json(['errors' => $e], 422);
    }


    protected function formatValidationError($title = null, $detail = null, $pointer = null)
    {
        $msgDetail = str_replace('attributes.', '', $detail);
        $pointer = str_replace('.', '/', '/data/'.$pointer);

        $errors = [
            'title' => $title,
            'detail' => $msgDetail
        ];

        if (! is_null($pointer)) {
            $errors = array_merge([
                'source' => [
                    'pointer' => $pointer
                ]
            ], $errors);
        }

        return $errors;
    }
}
