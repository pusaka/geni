<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Exceptions;

/**
 * InvalidFilterFieldException
 */
class InvalidFilterFieldException extends \Exception
{
    protected $invalidField;

    public function __construct($invalidField)
    {
        parent::__construct('Cannot filter using '.$invalidField.', field undefined in filterable fields.');

        $this->invalidField = $invalidField;
    }

    public function getInvalidField()
    {
        return $this->invalidField;
    }
}
