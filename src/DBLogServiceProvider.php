<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class DBLogServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (! Config::get('app.debug')) return;

        DB::listen(
            function ($sql) {
                foreach ($sql->bindings as $i => $binding) {
                    if ($binding instanceof \DateTime) {
                        $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    }
                    elseif (is_string($binding)) {
                        $sql->bindings[$i] = "'$binding'";
                    }
                }

                // Insert bindings into query
                $query = str_replace(array('%', '?'), array('%%', '%s'), $sql->sql);
                    
                if (count($sql->bindings) > 0) {
                    $query = vsprintf($query, $sql->bindings);
                }

                // Save the query to file
                $logFile = fopen(
                    storage_path('logs' . DIRECTORY_SEPARATOR . 'query_' . date('Y-m-d') . '.log'),
                    'a+'
                );
                fwrite($logFile,
                    '-- ['.date('Y-m-d H:i:s').'] Connection: '
                        .$sql->connectionName.' @'.$sql->time.'ms'.PHP_EOL
                        .$query.';'.PHP_EOL
                );
                fclose($logFile);
            }
        );
    }
}
