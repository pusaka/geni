<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni;

use Illuminate\Support\Facades\Route;
use Pusaka\Geni\Http\ApiController;
use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{
    /**
     * boot
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->app['router']->pushMiddlewareToGroup('api', \Barryvdh\Cors\HandleCors::class);

        Route::middleware('api')->get('/api', ApiController::class.'@index');
    }

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
