<?php /*** Bismillahirrahmanirrahim ***/
namespace Pusaka\Geni\Http;

use Illuminate\Routing\Controller as BaseController;

/**
 * ApiController
 */
class ApiController extends BaseController
{
    /**
     * summary
     */
    public function index()
    {
        return [
            'meta' => [
                'copyright' => 'eMpu Datanetics © 2018',
                'licensee' => [
                    'name' => 'Dishubkominfo Kota Surakarta',
                    'address' => 'Jl. Menteri Supeno No. 7, 57139',
                    'phone' => '(0271) 71740',
                    'email' => 'dishub@surakarta.go.id',
                    'website' => 'http://dishub.surakarta.go.id',
                ],
            ],
            'data' => null,
            'links' => [
                'ubkb-brands' => route('ubkb-brands.index'),
                'ubkb-caroseries' => route('ubkb-caroseries.index'),
                'ubkb-categories' => route('ubkb-categories.index'),
                'ubkb-designs' => route('ubkb-designs.index'),
                'ubkb-documents' => route('ubkb-documents.index'),
                'ubkb-files' => route('ubkb-files.index'),
                'ubkb-fuels' => route('ubkb-fuels.index'),
                'ubkb-legalentities' => route('ubkb-legalentities.index'),
                'ubkb-officers' => route('ubkb-officers.index'),
                'ubkb-regionaloffices' => route('ubkb-regionaloffices.index'),
                'ubkb-retributions' => route('ubkb-retributions.index'),
                'ubkb-services' => route('ubkb-services.index'),
                'ubkb-tankages' => route('ubkb-tankages.index'),
                'ubkb-vehicles' => route('ubkb-vehicles.index'),
            ]
        ];
    }

    public function name()
    {
        
    }
}
