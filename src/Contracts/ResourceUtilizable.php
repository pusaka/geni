<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Contracts;

interface ResourceUtilizable
{
    /**
     * Type key, name of resource entity
     * @return string
     */
    public function getTypeKey();
}
