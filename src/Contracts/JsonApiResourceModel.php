<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Contracts;

interface JsonApiResourceModel
{
    /**
     * Type key, name of resource entity
     * @return string
     */
    public function getTypeKey();
}
