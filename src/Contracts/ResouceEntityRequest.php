<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Contracts;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface ResourceEntityRequest
 * @package Pusaka\Geni
 */
interface ResourceEntityRequest
{
    /**
     * Desired resource type key
     * @return string
     */
    public function typeKey();
}
