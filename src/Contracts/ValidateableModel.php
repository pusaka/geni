<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Contracts;

interface ValidateableModel
{
    /**
     * Array of validation rule
     * @return array
     */
    public function validationRules();
}
